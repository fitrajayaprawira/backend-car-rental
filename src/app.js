const express = require("express");
const bodyParser = require("body-parser");
const ejs = require("ejs");
const routes = require("./routes/web");

const app = express();

const jsonParser = bodyParser.json();
const urlEncoded = bodyParser.urlencoded({ extended: false });

const path = require('path')
app.use(express.static(path.join(__dirname, 'public')))
app.use(jsonParser);
app.use(urlEncoded);

app.set("views", "./src/pages");
app.set("view engine", "ejs");
app.engine(".ejs", ejs.__express);

app.use("/", routes());

app.listen(3030, function () {
  console.log("Client App running on http://localhost:3030");
});