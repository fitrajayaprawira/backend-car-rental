class HomeController {
    index(req, res) {
      res.render("home");
    }
    account(req, res) {
      return res.render("account", {
        Username: req.Username
      });
    }
  }
  
  module.exports = new HomeController();
  