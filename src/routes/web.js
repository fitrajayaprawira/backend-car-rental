const express = require("express");
const AuthController = require("../controllers/AuthController");
const HomeController = require("../controllers/HomeController");

function routes() {
  const router = express.Router();

  router.get("/", function(req,res){
    HomeController.index(req,res);
  });

  router.get("/login", function (req, res) {
    AuthController.login(req,res);
  });

  router.get("/account", function (req, res) {
    HomeController.account(req,res);
  });

  return router;
}

module.exports = routes;